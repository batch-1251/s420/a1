import React,{useContext,useEffect, useState} from 'react';

import UserContext from './../UserContext';

import {Link, useParams, useHistory} from 'react-router-dom';

import {Container,Card, Button} from 'react-bootstrap'

import Swal from 'sweetalert2';




export default function SpecificCourse(){

	const[name, setName]=useState('');
	const[descirption,setDescription]=useState('');
	const[price, setPrice]=useState('')

	const {user}=useContext(UserContext);

	const {courseId}=useParams();

	let token = localStorage.getItem('token');

	let hitory=useHistory()

	useEffect(()=>{
		fetch(`http://localhost:4000/api/courses/${courseId}`,{
			method:"GET",
			headers:{
				"Content-Type":"application/json",
				"Authorization":`Bearer ${token}`
			}
		}).then(result=>result.json()).then(result=>{
			console.log(result)

			setName(result.name);
			setDescription(result.descirption);
			setPrice(result.price)
		})
	},[])

	const enroll=()=>{
		fetch('http://localhost:4000/api/users/enroll',{
			method:"POST",
			headers:{
				"Content-Type":"application/json",
				"Authorization":`Bearer ${token}`
			},
			body:JSON.stringify({
				courseId:courseId
			})
		}).then(result=>result.json()).then(result=>{
			console.log(result)
			if(result===true){
				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Enrolled successfully"
				})
				history.push('/courses')
			}else{
				Swal.fire({
					title: "Success",
					icon: "error",
					text: "Please try again"
				})
			}
		})
	}
	return(
			<Container>
				<Card>
					<Card.Header>
						<h4>
							{name}
						</h4>
					</Card.Header>
					<Card.Bod>
						<Card.Text>
							{descirption}
						</Card.Text>
						<h6>
							Price:Php
							<span className="mx-2">{price}</span>
						</h6>
					</Card.Bod>	
					<Card.Footer>
					{
						(user.id !==null)?<Button variant="primary" onSubmit={(e)=>enroll(e)}>Enroll</Button>
						: <Link className="btn btn-danger" to="/login">Login to Enroll</Link>
					}

					</Card.Footer>
				</Card>
			</Container>	
		)
}